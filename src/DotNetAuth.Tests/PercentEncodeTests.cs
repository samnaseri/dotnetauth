using DotNetAuth.OAuth1a.Framework;
using NUnit.Framework;

namespace DotNetAuth.OAuth1.Framework.Tests
{
    public class PercentEncodeTests
    {
        [TestFixture]
        public class EncodeMethod
        {
            [Test]
            public void IfAllInputCharactersAreAllowed_ShouldReturnSameString()
            {
                const string text = "text";
                const string expected = text;
                var result = PercentEncode.Encode(text);
                Assert.AreEqual(expected, result);
            }
            [Test]
            public void ShouldReplaceUnicodeCharactersInInput()
            {
                const string text = "متن";
                const string expected = "%D9%85%D8%AA%D9%86";
                var result = PercentEncode.Encode(text);
                Assert.AreEqual(expected, result);
            }
            [Test]
            public void ShouldReplaceNotAllowedSymbols()
            {
                const string text = @"^@()`""\/,*+";
                const string expected = "%5E%40%28%29%60%22%5C%2F%2C%2A%2B";
                var result = PercentEncode.Encode(text);
                Assert.AreEqual(expected, result);
            }
        }

        [TestFixture]
        public class DecodeMethod
        {
            [Test]
            public void IfAllInputCharactersAreAllowed_ShouldReturnSameString()
            {
                const string text = "text";
                const string expected = text;
                var result = PercentEncode.Decode(text);
                Assert.AreEqual(expected, result);
            }
            [Test]
            public void EscapedCharactersShouldBeRecovered()
            {
                const string text = "%5E%40";
                const string expected = "^@";
                var result = PercentEncode.Decode(text);
                Assert.AreEqual(expected, result);
            }
        }
    }
}
