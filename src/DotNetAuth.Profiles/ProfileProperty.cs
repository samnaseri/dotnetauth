namespace DotNetAuth.Profiles
{
    public class ProfileProperty
    {
        #region ctor
        public ProfileProperty(string name)
        {
            PropertyName = name;
        }
        public ProfileProperty(string name, string scope, string nativePropertyName, string endpointSettings)
        {
            PropertyName = name;
            RequiredScope = scope;
            EndpointSettings = endpointSettings;
            NativePropertyName = nativePropertyName;
        }
        #endregion
        #region properties
        public string PropertyName { get; private set; }
        public string RequiredScope { get; private set; }
        public string EndpointSettings { get; private set; }
        public string NativePropertyName { get; private set; }
        #endregion
        #region methods
        public ProfileProperty Scope(string scope)
        {
            return new ProfileProperty(PropertyName, scope, NativePropertyName, EndpointSettings);
        }
        public ProfileProperty Query(string settings)
        {
            return new ProfileProperty(PropertyName, RequiredScope, NativePropertyName, settings);
        }
        public ProfileProperty Result(string nativeName)
        {
            return new ProfileProperty(PropertyName, RequiredScope, nativeName, EndpointSettings);
        }
        #endregion
        #region static fields
        public static ProfileProperty UniqueID = new ProfileProperty("UniqueID");
        public static ProfileProperty FirstName = new ProfileProperty("FirstName");
        public static ProfileProperty LastName = new ProfileProperty("LastName");
        public static ProfileProperty FullName = new ProfileProperty("FullName");
        public static ProfileProperty DisplayName = new ProfileProperty("DisplayName");
        public static ProfileProperty Username = new ProfileProperty("Username");
        public static ProfileProperty ProfileLink = new ProfileProperty("ProfileLink");
        public static ProfileProperty PictureLink = new ProfileProperty("PictureLink");
        public static ProfileProperty Gender = new ProfileProperty("Gender");
        public static ProfileProperty BirthDate = new ProfileProperty("BirthDate");
        public static ProfileProperty Timezone = new ProfileProperty("Timezone");
        public static ProfileProperty Email = new ProfileProperty("Email");
        public static ProfileProperty Location = new ProfileProperty("Location");
        public static ProfileProperty Website = new ProfileProperty("Website");
        public static ProfileProperty Locale = new ProfileProperty("Locale");
        #endregion
    }
}
