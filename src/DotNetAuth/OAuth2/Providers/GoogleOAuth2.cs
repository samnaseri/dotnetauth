﻿using System.Collections.Generic;
using DotNetAuth.OAuth2.Framework;

namespace DotNetAuth.OAuth2.Providers
{
    /// <summary>
    /// OAuth 2 provider for Google accounts.
    /// </summary>
    public class GoogleOAuth2 : OAuth2ProviderDefinition
    {
        readonly bool offline;
        readonly bool forceApprovalPrompt;

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleOAuth2"/> class.
        /// </summary>
        public GoogleOAuth2()
        {
            AuthorizationEndpointUri = "https://accounts.google.com/o/oauth2/auth";
            TokenEndpointUri = "https://accounts.google.com/o/oauth2/token";
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleOAuth2"/> class.
        /// </summary>
        /// <param name="offline">if set to <c>true</c> you can use the refresh token to get new access token when your access token expires.</param>
        /// <param name="forceApprovalPrompt">if set to <c>true</c> even though user previously has permitted your application, the authentication page will be shown to him, so, for example, they can login using a different user.</param>
        public GoogleOAuth2(bool offline, bool forceApprovalPrompt)
        {
            this.offline = offline;
            this.forceApprovalPrompt = forceApprovalPrompt;
        }
        /// <summary>
        /// Indicates if the OAuth provider honours the state argument.
        /// </summary>
        /// <remarks>
        /// When implementing definition for a OAuth provider, you may override this property to specify the behaviour supported by current provider. If the provider does not return back the same state value after authentication, override and return false.
        /// </remarks>
        public override bool StateHonored
        {
            get { return true; }
        }
        /// <summary>
        /// Returns a list of parameters to be included in authorization endpoint as query string. 
        /// </summary>
        /// <param name="appCredentials">The user's application credentials.</param>
        /// <param name="redirectUri">The redirect URI in which OAuth user wishes sites user to be returned to finally</param>
        /// <param name="scope">The scope of access or set of permissions OAuth user is demanding.</param>
        /// <param name="stateManager">An implementation of <see cref="IOAuth20StateManager"/> for providing state value.</param>
        /// <returns>A list of parameters to be included in authorization endpoint.</returns>
        public override ParameterSet GetAuthorizationRequestParameters(ApplicationCredentials appCredentials, string redirectUri, string scope, IOAuth20StateManager stateManager)
        {
            return new ParameterSet(new Dictionary<string, string> {
                {"client_id", appCredentials.AppId},
                {"redirect_uri", redirectUri},
                {"scope", scope},
                {"state", stateManager != null ? stateManager.GetState() : null},
                {"response_type", "code"},
                {"access_type", offline ? "offline" : "online"},
                {"approval_prompt", forceApprovalPrompt ? "force" : "auto"},
            });
        }
    }
}
