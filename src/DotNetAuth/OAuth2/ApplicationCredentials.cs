
namespace DotNetAuth.OAuth2
{
    /// <summary>
    /// Encapsulates a set of keys which is required to identify your application. 
    /// </summary>
    /// <remarks>
    /// <para>
    /// For each OAuth 2 provider you need to define an application through their developers dashboard.
    /// Once you define your application you will be provided with a set of App Id and App Secret keys. 
    /// This class encapsulates those keys.
    /// </para>
    /// <para>
    /// This class simply encapsulates values for a set of App ID and App Secret ID. 
    /// To retrieve these values you need to register an application and once you complete the registration successfully you will be given these values.</para>
    /// <para>
    /// You should not reveal these values publicly. To be able to change them you probably will put these values
    /// into a configuration file. It is suggested to encrypt these values while saving in a configuration file to avoid
    /// those values being accessible to public.
    /// </para>
    /// <para>
    /// In OAuth 2 specification 'Application' is being referred to as 'Client' as from
    /// OAuth 2 point of view an application is a client for authentication providers.
    /// </para>    
    /// <para>
    /// <h4>OAuth 1.0a</h4>
    /// The type <see cref="DotNetAuth.OAuth1a.ApplicationCredentials"/> plays the same role for OAuth 1.0a
    /// </para>
    /// </remarks>
    public class ApplicationCredentials
    {
        /// <summary>
        /// A unique ID of the application. 
        /// From OAuth2 specification point of view this is commonly called as 'client_id'.
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// An ID which should kept secret and works as a confirmation that you are making
        /// valid requests as the application.
        /// </summary>
        /// <remarks>
        /// <para>
        /// From OAuth2 specification point of view this is commonly called as 'client_secret'.
        /// Most providers(all that I know) allows you to change client_secret, so do it regularly to 
        /// make sure that your application is not comprised.
        /// </para>
        /// </remarks>
        public string AppSecretId { get; set; }
    }
}
