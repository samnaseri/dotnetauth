﻿namespace DotNetAuth.OAuth2.Providers
{
    /// <summary>
    /// OAuth 2 provider for Microsoft Account.
    /// </summary>
    public class LiveOAuth2 : OAuth2ProviderDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LiveOAuth2"/> class.
        /// </summary>
        public LiveOAuth2()
        {
            AuthorizationEndpointUri = "https://login.live.com/oauth20_authorize.srf";
            TokenEndpointUri = "https://login.live.com/oauth20_token.srf";
        }
    }
}
