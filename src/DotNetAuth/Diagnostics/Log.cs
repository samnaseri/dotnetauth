using System;
using System.Collections.Generic;

namespace DotNetAuth.Diagnostics
{
    /// <summary>
    /// An interface to plug a logging library.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Receives a log message.
        /// </summary>
        /// <param name="level">The level of severity of message.</param>
        /// <param name="message">The message.</param>
        void Log(LogLevel level, string message);
        /// <summary>
        /// Receives an exception to log it.
        /// </summary>
        /// <param name="ex">The occurred exception.</param>        
        void Log(Exception ex);
        /// <summary>
        /// Receive a redirection action info to log it.
        /// </summary>
        /// <param name="redirect">The redirection info.</param>
        void Log(Redirect redirect);
        /// <summary>
        /// Receives a http request info to log it.
        /// </summary>
        /// <param name="request">The http request to log.</param>
        void Log(HttpRequest request);
        /// <summary>
        /// Receives a http response to log it.
        /// </summary>
        /// <param name="response"></param>
        void Log(HttpResponse response);
    }
    /// <summary>
    /// Use <see cref="Log.Logger"/> to specify a custom <see cref="ILogger"/> object to receive messages and exception.
    /// </summary>
    public static class Log
    {
        static ILogger logger = new DefaultLogger();
        /// <summary>
        /// Gets or sets implementation of <see cref="ILogger"/> to receive logging via custom implementation.
        /// </summary>
        public static ILogger Logger
        {
            get { return logger; }
            set
            {
                logger = value;
            }
        }
        internal static void Trace(string message)
        {
            logger.Log(LogLevel.Trace, message);
        }
        internal static void Info(string message)
        {
            logger.Log(LogLevel.Info, message);
        }
        internal static void Warnning(string message)
        {
            logger.Log(LogLevel.Warnning, message);
        }
        internal static void Error(string message)
        {
            logger.Log(LogLevel.Error, message);
        }
        internal static void OutgoingRedirect(string url)
        {
            logger.Log(new Redirect { Url = new Uri(url), Initiator = Redirect.RedirectSource.OAuthConsumer });
        }
        internal static void IncomingRedirect(string url)
        {
            logger.Log(new Redirect { Url = new Uri(url), Initiator = Redirect.RedirectSource.OAuthProvider });
        }
        internal static void OutgoingHttpRequest(string httpVerb, string uri, string body, Dictionary<string, string> headers)
        {
            logger.Log(new HttpRequest { HttpVerb = httpVerb, Url = new Uri(uri), Body = body, Headers = headers });
        }
        internal static void IncommingHttpResponse(string body, Dictionary<string, string> headers)
        {
            logger.Log(new HttpResponse { Body = body, Headers = headers });
        }
    }
    internal class DefaultLogger : ILogger
    {
        public void Log(LogLevel level, string message)
        {
        }
        public void Log(Exception ex)
        {
        }
        public void Log(Redirect redirect)
        {
        }
        public void Log(HttpRequest request)
        {
        }
        public void Log(HttpResponse response)
        {
        }
    }
    /// <summary>
    /// Severity of logging message.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// For tracing purposes.
        /// </summary>
        Trace = 0,
        /// <summary>
        /// General message.
        /// </summary>
        Info = 1,
        /// <summary>
        /// A message about something undesired, however there is chances it still work.
        /// </summary>
        Warnning = 2,
        /// <summary>
        /// A message about a problem preventing DotNetAuth from working well.
        /// </summary>
        Error = 3,
    }
    /// <summary>
    /// Encapsulate information about HTTP redirect actions that happens during OAuth protocol.
    /// </summary>
    /// <remarks>
    /// OAuth protocol is a protcol between three parties: user, provider, consumer. User will be redirected 
    /// to provider by consumer and then redirected back to consumer by provider. So this Redirect objects 
    /// hold information about details of those redirect actions.
    /// </remarks>
    public class Redirect
    {
        /// <summary>
        /// Gets the Url which user redirected to.
        /// </summary>
        public Uri Url { get; internal set; }
        /// <summary>
        /// Gets the party which initiated the redirection.
        /// </summary>
        public RedirectSource Initiator { get; set; }
        /// <summary>
        /// Parties which may redirect user.
        /// </summary>
        public enum RedirectSource
        {
            /// <summary>
            /// OAuth Consumer.
            /// </summary>
            OAuthConsumer,
            /// <summary>
            /// OAuth Provider.
            /// </summary>
            OAuthProvider,
        }
    }
    /// <summary>
    /// Encapsulates OAuth relevant HTTP request info.
    /// </summary>
    public class HttpRequest
    {
        /// <summary>
        /// Gets the HTTP verb which was used.
        /// </summary>
        public string HttpVerb { get; internal set; }
        /// <summary>
        /// Gets the Url which requested.
        /// </summary>
        public Uri Url { get; internal set; }
        /// <summary>
        /// Gets the body of request.
        /// </summary>
        public string Body { get; internal set; }
        /// <summary>
        /// Gets the Headers of request.
        /// </summary>
        public Dictionary<string, string> Headers { get; internal set; }
    }
    /// <summary>
    /// Encapsulates a HTTP response.
    /// </summary>
    public class HttpResponse
    {
        /// <summary>
        /// Gets the body of response.
        /// </summary>
        public string Body { get; internal set; }
        /// <summary>
        /// Gets the headers of response.
        /// </summary>
        public Dictionary<string, string> Headers { get; internal set; }
    }
}
