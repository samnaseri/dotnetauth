﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
namespace DotNetAuth.Profiles.MVC
{

    public class OAuthAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(System.Web.HttpContextBase httpContext)
        {
            return base.AuthorizeCore(httpContext);

            var formsIdentity = httpContext.User.Identity as FormsIdentity;
            

            

        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
    public class AuthenticationManager
    {
        public static void RegisterRoutes(RouteCollection routes, string initPath, string processResponsePath)
        {
            routes.MapRoute("OAuthInit",
                initPath,
                new { controller = "AuthenticationController", action = "Init" },
                new { },
                new[] { "DotNetAuth.Profiles.MVC" });
            routes.MapRoute("OAuthProcessResponse",
                processResponsePath,
                new { controller = "AuthenticationController", action = "CompleteAuthentication" },
                new { },
                new[] { "DotNetAuth.Profiles.MVC" });
        }
    }

    public class AuthenticationController : Controller
    {
        private static readonly ProfileProperty[] properties = new[] { ProfileProperty.UniqueID };
        public RedirectResult Init(string providerName)
        {
            var provider = LoginProvider.Get(providerName);
            var redirectUri = new Uri("");
            var stateManager = new DefaultLoginStateManager(Session);
            var response = Login.GetAuthenticationUri(provider, redirectUri, stateManager, properties);
            response.Wait();
            return new RedirectResult(response.Result.AbsolutePath);
        }
        public void CompleteAuthentication(string providerName)
        {
            var response = Login.GetProfile(null, Request.Url, "", null, properties);
            response.Wait();
            var profile = response.Result;
            FormsAuthentication.SetAuthCookie(providerName + "//" + profile.UniqueID, true);

        }
    }
}
