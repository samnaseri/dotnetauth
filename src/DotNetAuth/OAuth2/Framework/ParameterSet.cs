﻿using System.Collections.Generic;
using System.Linq;

namespace DotNetAuth.OAuth2.Framework
{
    /// <summary>
    /// Encapsulates a set of parameters.
    /// </summary>
    /// <remarks>
    /// This class is basically keeps a list of <see cref="Parameter"/>s.
    /// </remarks>
    public class ParameterSet
    {
        private readonly List<Parameter> list;

        #region ctor
        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterSet"/> class.
        /// </summary>
        public ParameterSet()
        {
            list = new List<Parameter>();
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ParameterSet"/> class with initial parameters.
        /// </summary>
        /// <param name="initialValues">The initial values.</param>
        public ParameterSet(Dictionary<string, string> initialValues)
            : this()
        {
            foreach (var i in initialValues)
                Add(i.Key, i.Value);
        }
        #endregion

        #region indexer
        /// <summary>
        /// Gets the value of parameter with the specified name.
        /// </summary>
        public string this[string name]
        {
            get
            {
                return list.Where(i => i.Name == name).Select(i => i.Value).SingleOrDefault();
            }
        }
        #endregion

        #region methods
        /// <summary>
        /// Adds the specified parameter.
        /// </summary>
        /// <param name="parameter">The parameter to be added.</param>
        public void Add(Parameter parameter)
        {
            list.Add(parameter);
        }
        /// <summary>
        /// Adds a new parameter by it's name and value.
        /// </summary>
        /// <param name="name">The name of parameter.</param>
        /// <param name="value">The value of parameter.</param>
        public void Add(string name, string value)
        {
            var item = new Parameter(name, value);
            list.Add(item);
        }
        /// <summary>
        /// Adds a list of parameters.
        /// </summary>
        /// <param name="newList">The new list.</param>
        public void Add(IEnumerable<Parameter> newList)
        {
            list.AddRange(newList);
        }
        /// <summary>
        /// Gets the list of all available parameters.
        /// </summary>
        /// <returns></returns>
        public Parameter[] Get()
        {
            return list.ToArray();
        }
        #endregion
    }
}
