﻿namespace DotNetAuth.OAuth2.Providers
{
    /// <summary>
    /// OAuth 2 provider for Meetup.
    /// </summary>
    public class MeetupOAuth2 : OAuth2ProviderDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MeetupOAuth2"/> class.
        /// </summary>
        public MeetupOAuth2()
        {
            AuthorizationEndpointUri = "https://secure.meetup.com/oauth2/authorize";
            TokenEndpointUri = "https://secure.meetup.com/oauth2/access";
        }
    }
}
