namespace DotNetAuth.Profiles
{
    public class TwitterLoginProviderDefinition : LoginProviderDefinition
    {
        readonly ProfileProperty[] supportedProperties;
        public TwitterLoginProviderDefinition()
        {
            Name = "Twitter";
            Fullname = "Twitter";
            ProtocolType = ProtocolTypes.OAuth1a;
            supportedProperties = new[] {
                ProfileProperty.UniqueID,
                ProfileProperty.Website,
                ProfileProperty.ProfileLink,
                ProfileProperty.DisplayName,
                ProfileProperty.FullName,
                ProfileProperty.PictureLink,
                ProfileProperty.Timezone,
                ProfileProperty.Username,
                ProfileProperty.Location
            };
        }
        public override ProfileProperty[] GetSupportedProperties()
        {
            return supportedProperties;
        }
        public override string GetProfileEndpoint(ProfileProperty[] requiredProperties)
        {
            return "http://api.twitter.com/1/account/verify_credentials.json";
        }
        public override string GetRequiredScope(ProfileProperty[] requiredProperties)
        {
            return "";
        }
        public override Profile ParseProfile(string content)
        {
            var json = Newtonsoft.Json.Linq.JObject.Parse(content);
            var profile = new Profile {
                UniqueID = json.Value<string>("id"),
                Website = json.Value<string>("url"),
                ProfileLink = "http://twitter.com/@" + json.Value<string>("screen_name"),
                DisplayName = json.Value<string>("name"),
                FullName = json.Value<string>("name"),
                PictureLink = json.Value<string>("profile_image_url"),
                Timezone = json.Value<string>("time_zone"),
                Username = json.Value<string>("screen_name"),
                Location = json.Value<string>("location")
            };
            return profile;
        }
        public override OAuth1a.OAuth1aProviderDefinition GetOAuth1aDefinition()
        {
            return new OAuth1a.Providers.TwitterOAuth1a();
        }
    }
}
