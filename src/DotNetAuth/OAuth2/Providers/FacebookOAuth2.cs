using System;
using System.Linq;
using DotNetAuth.OAuth2.Framework;
using RestSharp.Contrib;

namespace DotNetAuth.OAuth2.Providers
{
    /// <summary>
    /// OAuth 2 provider for Facebook.
    /// </summary>
    public class FacebookOAuth2 : OAuth2ProviderDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookOAuth2"/> class.
        /// </summary>
        public FacebookOAuth2()
        {
            AuthorizationEndpointUri = "https://www.facebook.com/dialog/oauth";
            TokenEndpointUri = "https://graph.facebook.com/oauth/access_token";
        }
        /// <summary>
        /// Parses the response body to request to access token.
        /// </summary>
        /// <param name="body">The response body of request for access token.</param>
        /// <returns>A <see cref="ProcessUserResponseOutput"/> object containing access token and some additional parameters or error details.</returns>
        public override ProcessUserResponseOutput ParseAccessTokenResult(string body)
        {
            var values = HttpUtility.ParseQueryString(body);
            var allParameters = new ParameterSet();
            foreach (var v in values.Keys.Cast<string>()) allParameters.Add(v, values[v]);
            var accessToken = values["access_token"];
            DateTime? expires = null;
            if (null != values["expires"])
                expires = DateTime.Now.AddSeconds(int.Parse(values["expires"]));
            return new ProcessUserResponseOutput(allParameters, accessToken, expires);
        }
    }
}